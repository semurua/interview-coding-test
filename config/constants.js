const PRODUCT_NAMES = {
  LOW_COVERAGE: 'Low Coverage',
  MEDIUM_COVERAGE: 'Medium Coverage',
  FULL_COVERAGE: 'Full Coverage',
  SPECIAL_FULL_COVERAGE: 'Special Full Coverage',
  MEGA_COVERAGE: 'Mega Coverage',
  SUPER_SALE: 'Super Sale',
};

const MAX_PRODUCT_PRICE = 50;
const MIN_PRODUCT_PRICE = 0;

const MEGA_COVERAGE_PRICE = 80;

module.exports = {
  PRODUCT_NAMES,
  MAX_PRODUCT_PRICE,
  MIN_PRODUCT_PRICE,
  MEGA_COVERAGE_PRICE,
};
