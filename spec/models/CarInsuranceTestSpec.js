const expect = require('chai').expect;
const {random, times} = require('lodash');
const {CarInsurance} = require('../../src/models');
const sinon = require('sinon');

describe('Car Insurance', () => {
  describe('updatePrice', () => {
    it('should call all products updatePrice', () => {
      // setUp
      const numberOfProducts = random(1, 1000);
      const productsMock = times(numberOfProducts, () => ({updatePrice: sinon.stub().returnsThis()}));
      const carInsurance = new CarInsurance(productsMock);

      // exercise
      const result = carInsurance.updatePrice();

      // verify
      expect(result).deep.equal(productsMock);
      productsMock.forEach((productMock) => {
        expect(productMock.updatePrice.calledOnce).equal(true);
      });
    });

    it('should return empty array if undefined is passed as constructor param', () => {
      // setUp
      const carInsurance = new CarInsurance();

      // exercise
      const result = carInsurance.updatePrice();

      // verify
      expect(result.length).equal(0);
    });
  });
});
