const expect = require('chai').expect;
const {random} = require('lodash');
const {PRODUCT_NAMES, MAX_PRODUCT_PRICE} = require('../../config/constants');
const {SuperSaleProduct} = require('../../src/models');

describe('Super Sale Product', () => {
  describe('updatePrice', () => {
    it('should subtract sellIn one and price by two when price is higher than 0', () => {
      // setUp
      const initialSellIn = random(1, 100);
      const initialPrice = random(2, MAX_PRODUCT_PRICE);
      const superSaleProduct = new SuperSaleProduct(initialSellIn, initialPrice);

      // exercise
      const result = superSaleProduct.updatePrice();

      // verify
      expect(result.name).equal(PRODUCT_NAMES.SUPER_SALE);
      expect(result.sellIn).equal(initialSellIn - 1, 'Expected to subtract one to SellIn');
      expect(result.price).equal(initialPrice - 2, 'Expected to subtract one to Price');
    });

    it('should subtract one sellIn and do nothing with price when it is 0', () => {
      // setUp
      const initialSellIn = random(1, 100);
      const initialPrice = 0;
      const superSaleProduct = new SuperSaleProduct(initialSellIn, initialPrice);

      // exercise
      const result = superSaleProduct.updatePrice();

      // verify
      expect(result.name).equal(PRODUCT_NAMES.SUPER_SALE);
      expect(result.sellIn).equal(initialSellIn - 1, 'Expected to subtract one to SellIn');
      expect(result.price).equal(initialPrice, 'Expected to maintain Price');
    });

    it('should subtract four to price when sell in is 0 or lower', () => {
      // setUp
      const initialSellIn = random(0);
      const initialPrice = random(4, MAX_PRODUCT_PRICE);
      const superSaleProduct = new SuperSaleProduct(initialSellIn, initialPrice);

      // exercise
      const result = superSaleProduct.updatePrice();

      // verify
      expect(result.name).equal(PRODUCT_NAMES.SUPER_SALE);
      expect(result.sellIn).equal(initialSellIn - 1, 'Expected to subtract one to SellIn');
      expect(result.price).equal(initialPrice - 4, 'Expected to subtract two to Price');
    });
  });
});
