const expect = require('chai').expect;
const {random} = require('lodash');
const {PRODUCT_NAMES, MAX_PRODUCT_PRICE} = require('../../config/constants');
const {LowCoverageProduct} = require('../../src/models');

describe('Low Coverage Product', () => {
  describe('updatePrice', () => {
    it('should subtract sellIn and price by one when price is higher than 0', () => {
      // setUp
      const initialSellIn = random(1, 100);
      const initialPrice = random(1, MAX_PRODUCT_PRICE);
      const lowCoverageProduct = new LowCoverageProduct(initialSellIn, initialPrice);

      // exercise
      const result = lowCoverageProduct.updatePrice();

      // verify
      expect(result.name).equal(PRODUCT_NAMES.LOW_COVERAGE);
      expect(result.sellIn).equal(initialSellIn - 1, 'Expected to subtract one to SellIn');
      expect(result.price).equal(initialPrice - 1, 'Expected to subtract one to Price');
    });

    it('should subtract one sellIn and do nothing with price when it is 0', () => {
      // setUp
      const initialSellIn = random(1, 100);
      const initialPrice = 0;
      const lowCoverageProduct = new LowCoverageProduct(initialSellIn, initialPrice);

      // exercise
      const result = lowCoverageProduct.updatePrice();

      // verify
      expect(result.name).equal(PRODUCT_NAMES.LOW_COVERAGE);
      expect(result.sellIn).equal(initialSellIn - 1, 'Expected to subtract one to SellIn');
      expect(result.price).equal(initialPrice, 'Expected to maintain Price');
    });

    it('should subtract two to price when sell in is 0 or lower', () => {
      // setUp
      const initialSellIn = random(0);
      const initialPrice = random(2, MAX_PRODUCT_PRICE);
      const lowCoverageProduct = new LowCoverageProduct(initialSellIn, initialPrice);

      // exercise
      const result = lowCoverageProduct.updatePrice();

      // verify
      expect(result.name).equal(PRODUCT_NAMES.LOW_COVERAGE);
      expect(result.sellIn).equal(initialSellIn - 1, 'Expected to subtract one to SellIn');
      expect(result.price).equal(initialPrice - 2, 'Expected to subtract two to Price');
    });
  });
});
