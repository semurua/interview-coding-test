const expect = require('chai').expect;
const {random} = require('lodash');
const {PRODUCT_NAMES, MEGA_COVERAGE_PRICE} = require('../../config/constants');
const {MegaCoverageProduct} = require('../../src/models');

describe('Mega Coverage Product', () => {
  describe('updatePrice', () => {
    const initialSellIn = random(100);

    it('should do nothing with the price and sellIn. The price should always be 80', () => {
      // setUp
      const megaCoverageProduct = new MegaCoverageProduct(initialSellIn);

      // exercise
      const result = megaCoverageProduct.updatePrice();

      // verify
      expect(result.name).equal(PRODUCT_NAMES.MEGA_COVERAGE);
      expect(result.sellIn).equal(initialSellIn, 'Expected to maintain SellIn');
      expect(result.price).equal(MEGA_COVERAGE_PRICE, 'Expected to maintain Price in 80');
    });
  });
});
