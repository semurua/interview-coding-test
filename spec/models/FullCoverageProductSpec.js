const expect = require('chai').expect;
const {random} = require('lodash');
const {PRODUCT_NAMES, MAX_PRODUCT_PRICE} = require('../../config/constants');
const {FullCoverageProduct} = require('../../src/models');

describe('Full Coverage Product', () => {
  describe('updatePrice', () => {
    it('should subtract one to sellIn and add one to price when the price is lower than 50', () => {
      // setUp
      const initialSellIn = random(1, 100);
      const initialPrice = random(1, MAX_PRODUCT_PRICE - 1);
      const fullCoverageProduct = new FullCoverageProduct(initialSellIn, initialPrice);

      // exercise
      const result = fullCoverageProduct.updatePrice();

      // verify
      expect(result.name).equal(PRODUCT_NAMES.FULL_COVERAGE);
      expect(result.sellIn).equal(initialSellIn - 1, 'Expected to subtract one to SellIn');
      expect(result.price).equal(initialPrice + 1, 'Expected to add one to Price');
    });

    it('should add two to price when the price is lower than 50 and sellIn 0 or lower', () => {
      // setUp
      const initialSellIn = random(0);
      const initialPrice = random(1, MAX_PRODUCT_PRICE - 2);
      const fullCoverageProduct = new FullCoverageProduct(initialSellIn, initialPrice);

      // exercise
      const result = fullCoverageProduct.updatePrice();

      // verify
      expect(result.name).equal(PRODUCT_NAMES.FULL_COVERAGE);
      expect(result.sellIn).equal(initialSellIn - 1, 'Expected to subtract one to SellIn');
      expect(result.price).equal(initialPrice + 2, 'Expected to add two to Price');
    });

    it('should subtract one to sellIn and do nothing to price when it is 50', () => {
      // setUp
      const initialSellIn = random(1, 100);
      const fullCoverageProduct = new FullCoverageProduct(initialSellIn, MAX_PRODUCT_PRICE);

      // exercise
      const result = fullCoverageProduct.updatePrice();

      // verify
      expect(result.name).equal(PRODUCT_NAMES.FULL_COVERAGE);
      expect(result.sellIn).equal(initialSellIn - 1, 'Expected to subtract one to SellIn');
      expect(result.price).equal(MAX_PRODUCT_PRICE, 'Expected to maintain Price');
    });

    // Refactored this case because it was against business definition: The price of a product is never more than 50
    it('should subtract one to sellIn and do nothing to price when it higher than 50', () => {
      // setUp
      const initialSellIn = random(1, 100);
      const initialPrice = random(MAX_PRODUCT_PRICE, 100);
      const fullCoverageProduct = new FullCoverageProduct(initialSellIn, initialPrice);

      // exercise
      const result = fullCoverageProduct.updatePrice();

      // verify
      expect(result.name).equal(PRODUCT_NAMES.FULL_COVERAGE);
      expect(result.sellIn).equal(initialSellIn - 1, 'Expected to subtract one to SellIn');
      expect(result.price).equal(MAX_PRODUCT_PRICE, 'Expected to maintain Price');
    });
  });
});
