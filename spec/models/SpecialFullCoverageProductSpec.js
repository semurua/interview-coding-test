const expect = require('chai').expect;
const {random} = require('lodash');
const {PRODUCT_NAMES, MAX_PRODUCT_PRICE} = require('../../config/constants');
const {SpecialFullCoverageProduct} = require('../../src/models');

describe('Special Full Coverage Product', () => {
  describe('updatePrice', () => {
    it('should increase price by one when sellIn is higher than 10', () => {
      // setUp
      const initialSellIn = random(11, 100);
      const initialPrice = random(1, MAX_PRODUCT_PRICE - 1);
      const specialFullCoverageProduct = new SpecialFullCoverageProduct(initialSellIn, initialPrice);

      // exercise
      const result = specialFullCoverageProduct.updatePrice();

      // verify
      expect(result.name).equal(PRODUCT_NAMES.SPECIAL_FULL_COVERAGE);
      expect(result.sellIn).equal(initialSellIn - 1, 'Expected to subtract one to SellIn');
      expect(result.price).equal(initialPrice + 1, 'Expected to add one to Price');
    });

    it('should increase price by two when sellIn is between 6 and 10', () => {
      // setUp
      const initialSellIn = random(6, 10);
      const initialPrice = random(1, MAX_PRODUCT_PRICE - 2);
      const specialFullCoverageProduct = new SpecialFullCoverageProduct(initialSellIn, initialPrice);

      // exercise
      const result = specialFullCoverageProduct.updatePrice();

      // verify
      expect(result.name).equal(PRODUCT_NAMES.SPECIAL_FULL_COVERAGE);
      expect(result.sellIn).equal(initialSellIn - 1, 'Expected to subtract one to SellIn');
      expect(result.price).equal(initialPrice + 2, 'Expected to add two to Price');
    });

    it('should increase price by three when sellIn is between 1 and 5', () => {
      // setUp
      const initialSellIn = random(1, 5);
      const initialPrice = random(1, MAX_PRODUCT_PRICE - 3);
      const specialFullCoverageProduct = new SpecialFullCoverageProduct(initialSellIn, initialPrice);

      // exercise
      const result = specialFullCoverageProduct.updatePrice();

      // verify
      expect(result.name).equal(PRODUCT_NAMES.SPECIAL_FULL_COVERAGE);
      expect(result.sellIn).equal(initialSellIn - 1, 'Expected to subtract one to SellIn');
      expect(result.price).equal(initialPrice + 3, 'Expected to add three to Price');
    });

    it('should increase drop the price to 0 when the sellIn is less than 0', () => {
      // setUp
      const initialSellIn = random(0);
      const initialPrice = random(1, MAX_PRODUCT_PRICE - 1);
      const specialFullCoverageProduct = new SpecialFullCoverageProduct(initialSellIn, initialPrice);

      // exercise
      const result = specialFullCoverageProduct.updatePrice();

      // verify
      expect(result.name).equal(PRODUCT_NAMES.SPECIAL_FULL_COVERAGE);
      expect(result.sellIn).equal(initialSellIn - 1, 'Expected to subtract one to SellIn');
      expect(result.price).equal(0, 'Expected the Price to drop to 0');
    });

    it('should never increase the price over 50', () => {
      // setUp
      const initialSellIn = random(1, 100);
      const specialFullCoverageProduct = new SpecialFullCoverageProduct(initialSellIn, MAX_PRODUCT_PRICE);

      // exercise
      const result = specialFullCoverageProduct.updatePrice();

      // verify
      expect(result.name).equal(PRODUCT_NAMES.SPECIAL_FULL_COVERAGE);
      expect(result.sellIn).equal(initialSellIn - 1, 'Expected to subtract one to SellIn');
    });

    it('should never increase the price over when it is 50', () => {
      // setUp
      const initialSellIn = random(1, 100);
      const specialFullCoverageProduct = new SpecialFullCoverageProduct(initialSellIn, MAX_PRODUCT_PRICE);

      // exercise
      const result = specialFullCoverageProduct.updatePrice();

      // verify
      expect(result.name).equal(PRODUCT_NAMES.SPECIAL_FULL_COVERAGE);
      expect(result.sellIn).equal(initialSellIn - 1, 'Expected to subtract one to SellIn');
      expect(result.price).equal(MAX_PRODUCT_PRICE, 'Expected the Price to never go higher than 50');
    });

    // Refactored this case because it was against business definition: The price of a product is never more than 50
    it('should never increase the price over 50', () => {
      // setUp
      const initialSellIn = random(1, 100);
      const initialPrice = random(MAX_PRODUCT_PRICE + 1, 100);
      const specialFullCoverageProduct = new SpecialFullCoverageProduct(initialSellIn, initialPrice);

      // exercise
      const result = specialFullCoverageProduct.updatePrice();

      // verify
      expect(result.name).equal(PRODUCT_NAMES.SPECIAL_FULL_COVERAGE);
      expect(result.sellIn).equal(initialSellIn - 1, 'Expected to subtract one to SellIn');
      expect(result.price).equal(MAX_PRODUCT_PRICE, 'Expected to maintain the price when it is higher than 50');
    });
  });
});
