const {
  PRODUCT_NAMES,
} = require('../../config/constants');
const Product = require('./Product');

/**
 * Super sale product.
 */
class SuperSaleProduct extends Product {
  /**
   * SuperSaleProduct. Uses product constructor with SuperSaleProduct name.
   * @param {number} sellIn Value which denotes the number of days we have to sell the product.
   * @param {number} price Value which denotes how much the product cost.
   */
  constructor(sellIn, price) {
    super(PRODUCT_NAMES.SUPER_SALE, sellIn, price);
  }

  /**
   * Updates the price of the product. Following rules:
   * - subtract 1 to sellIn
   * - subtract 2 to price if sellIn > 1 else 4
   * @return {Product}
   */
  updatePrice() {
    return super.updatePrice(-2);
  }
}

module.exports = SuperSaleProduct;
