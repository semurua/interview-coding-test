/**
 * Represent an car insurance. It contains all the associated products.
 */
class CarInsurance {
  /**
   * Car insurance constructor.
   * @param {[Product]} products
   */
  constructor(products = []) {
    this.products = products;
  }

  /**
   * Update the price of all the products that it contains according to the name.
   * @return {Product[]|*}
   */
  updatePrice() {
    return this.products.map((product) => product.updatePrice());
  }
}

module.exports = CarInsurance;
