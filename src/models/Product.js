const {update} = require('lodash');
const {
  MIN_PRODUCT_PRICE,
  MAX_PRODUCT_PRICE,
} = require('../../config/constants');

/**
 * Represent a business product.
 */
class Product {
  /**
   * Product constructor.
   * @param {string} name The product name.
   * @param {number} sellIn Value which denotes the number of days we have to sell the product.
   * @param {number} price Value which denotes how much the product cost.
   */
  constructor(name, sellIn, price) {
    this.name = name;
    this.sellIn = sellIn;
    this.price = price;
  }

  /**
   * Updates the price of the product. Following default rules:
   * - subtract 1 to sellIn
   * - subtract 1 to price if sellIn > 1 else 2
   * @param {number} priceModifier the priceModifier to apply. Would be a nice idea to have it in the constructor, but I shouldn't modify it.
   * @param {boolean} useSellIn define if uses the condition for sellIn.
   * @return {Product}
   */
  updatePrice(priceModifier = -1, useSellIn = true) {
    const self = this;
    update(self, 'price', (price) => {
      let updatedPrice = price + priceModifier;
      if (useSellIn && self.sellIn <= 0) updatedPrice += priceModifier;
      return Math.min(Math.max(updatedPrice, MIN_PRODUCT_PRICE), MAX_PRODUCT_PRICE);
    });
    self.sellIn -= 1;
    return self;
  }
}

module.exports = Product;
