const {
  PRODUCT_NAMES,
} = require('../../config/constants');
const Product = require('./Product');

/**
 * Medium Coverage Product.
 */
class MediumCoverageProduct extends Product {
  /**
   * MediumCoverageProduct. Uses product constructor with MediumCoverageProduct name.
   * @param {number} sellIn Value which denotes the number of days we have to sell the product.
   * @param {number} price Value which denotes how much the product cost.
   */
  constructor(sellIn, price) {
    super(PRODUCT_NAMES.MEDIUM_COVERAGE, sellIn, price);
  }
}

module.exports = MediumCoverageProduct;
