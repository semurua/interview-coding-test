const {
  MEGA_COVERAGE_PRICE,
  PRODUCT_NAMES,
} = require('../../config/constants');
const Product = require('./Product');
/**
 * Mega Coverage Product. It is a Legendary product.
 */
class MegaCoverageProduct extends Product {
  /**
   * MegaCoverageProduct. Uses product constructor with MegaCoverageProduct name.
   * @param {number} sellIn Value which denotes the number of days we have to sell the product.
   */
  constructor(sellIn) {
    super(PRODUCT_NAMES.MEGA_COVERAGE, sellIn, MEGA_COVERAGE_PRICE);
  }

  /**
   * Does nothing because is a legendary product.
   * @return {Product}
   */
  updatePrice() {
    return this;
  }
}

module.exports = MegaCoverageProduct;
