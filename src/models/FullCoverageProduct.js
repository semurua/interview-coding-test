const {
  PRODUCT_NAMES,
} = require('../../config/constants');
const Product = require('./Product');

/**
 * Full coverage product.
 */
class FullCoverageProduct extends Product {
  /**
   * FullCoverageProduct. Uses product constructor with FullCoverageProduct name.
   * @param {number} sellIn Value which denotes the number of days we have to sell the product.
   * @param {number} price Value which denotes how much the product cost.
   */
  constructor(sellIn, price) {
    super(PRODUCT_NAMES.FULL_COVERAGE, sellIn, price);
  }

  /**
   * Updates the price of the product. Following rules:
   * - subtract 1 to sellIn
   * - add 1 to price if sellIn > 1 else 2
   * @return {Product}
   */
  updatePrice() {
    return super.updatePrice(1);
  }
}

module.exports = FullCoverageProduct;
