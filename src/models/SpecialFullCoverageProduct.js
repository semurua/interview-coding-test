const {
  PRODUCT_NAMES,
} = require('../../config/constants');
const Product = require('./Product');

/**
 * Special Full coverage product.
 */
class SpecialFullCoverageProduct extends Product {
  /**
   * SpecialFullCoverageProduct. Uses product constructor with SpecialFullCoverageProduct name.
   * @param {number} sellIn Value which denotes the number of days we have to sell the product.
   * @param {number} price Value which denotes how much the product cost.
   */
  constructor(sellIn, price) {
    super(PRODUCT_NAMES.SPECIAL_FULL_COVERAGE, sellIn, price);
  }

  /**
   * Updates the price of the product. Following rules:
   * - subtract 1 to sellIn
   * - add 1 to price if sellIn > 10
   * - add 2 to price if 5 > sellIn <= 10
   * - add 3 to price if sellIn <= 5
   * - set price to 0 if sellIn <= 0
   * @return {Product}
   */
  updatePrice() {
    let priceModifier = 1;
    if (this.sellIn <= 10) ++priceModifier;
    if (this.sellIn <= 5) ++priceModifier;
    super.updatePrice(priceModifier, false);

    if (this.sellIn < 0) this.price = 0;
    return this;
  }
}


module.exports = SpecialFullCoverageProduct;
