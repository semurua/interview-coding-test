const path = require('path');
const fs = require('fs');

fs.readdirSync(__dirname).forEach((file) => {
  if (path.parse(file).ext === '.js' && path.parse(file).name !== 'index') {
    const name = file.replace('.js', '');
    exports[name] = require(`./${file}`);
  }
});
