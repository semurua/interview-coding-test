const {
  PRODUCT_NAMES,
} = require('../../config/constants');
const Product = require('./Product');

/**
 * Low Coverage Product.
 */
class LowCoverageProduct extends Product {
  /**
   * LowCoverageProduct. Uses product constructor with LowCoverageProduct name.
   * @param {number} sellIn Value which denotes the number of days we have to sell the product.
   * @param {number} price Value which denotes how much the product cost.
   */
  constructor(sellIn, price) {
    super(PRODUCT_NAMES.LOW_COVERAGE, sellIn, price);
  }
}

module.exports = LowCoverageProduct;
