const {times} = require('lodash');
const {
  LowCoverageProduct,
  MediumCoverageProduct,
  FullCoverageProduct,
  MegaCoverageProduct,
  SpecialFullCoverageProduct,
  SuperSaleProduct,
  CarInsurance,
} = require('./src/models');

const productsAtDayZero = [
  new MediumCoverageProduct(10, 20),
  new FullCoverageProduct(2, 0),
  new LowCoverageProduct(5, 7),
  new MegaCoverageProduct(0),
  new MegaCoverageProduct(-1),
  new SpecialFullCoverageProduct(15, 20),
  new SpecialFullCoverageProduct(10, 49),
  new SpecialFullCoverageProduct(5, 49),
  new SuperSaleProduct(3, 6),
];

const carInsurance = new CarInsurance(productsAtDayZero);
const productPrinter = (product) => {
  console.log(`${product.name}, ${product.sellIn}, ${product.price}`);
};
const printDay = (products, index) => {
  console.log(`-------- day ${index + 1 } --------`);
  console.log('name, sellIn, price');
  products.forEach(productPrinter);
  console.log('');
};

printDay(productsAtDayZero, -1);

times(30, (index) => printDay(carInsurance.updatePrice(), index));
