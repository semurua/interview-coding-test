# Summary
This is a test project for ComparaOnline, made by Sebastian Edgardo Murúa as a test.

## Commands

### Test
`$ npm test`

This command will also generate the coverage reports in `/converage` and show the result as console output. 
It can be checked with any web browser opening the file `/converage/index`

### Coverage
`$ npm run check-coverage`

This command will run the test and check the coverage from the result. It will fail if the thresholds are not met.

### Coverage
`$ npm run after-30-days`

This command will generate the desired file with the results after 30 days.
